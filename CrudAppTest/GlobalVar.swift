//
//  GlobalVar.swift
//  CrudAppTest
//
//  Created by Firman on 30/10/18.
//  Copyright © 2018 Firman App. All rights reserved.
//

import UIKit

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
var dictAPI = NSDictionary()
var arrayGlobalCallbackAPI = [[String : Any]]()
var arrayGlobalCallbackByIdAPI = [[String : Any]]()
var errorMessage = String()
var errorCodeString = String()

var bgTransparant = UIView.init()
var firstNameVal = UITextField.init()
var lastNameVal = UITextField.init()
var ageVal = UITextField.init()
var urlVal = UITextField.init()

extension UIViewController {
   
    // Button ==============
    func createButtonWithText(buttonColor : UIColor, buttonText : String, position : CGRect , buttonTag : Int) -> UIButton{
        let buttonCreate = UIButton.init()
        buttonCreate.frame = position
        buttonCreate.setTitle(buttonText, for: UIControlState.normal)
        buttonCreate.titleLabel?.font =  UIFont(name: "", size: 15)
        buttonCreate.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        buttonCreate.backgroundColor = buttonColor
        buttonCreate.layer.cornerRadius = 13
//        buttonCreate.layer.borderColor = UIColor.white.cgColor
//        buttonCreate.layer.borderWidth = 2;
        buttonCreate.tag = buttonTag
        
        return buttonCreate
    }
    
    func buttonAction(sender:UIButton!) {
        print("Button tapped not override")
    }
    
    // Label
    func createLabelWithText(color : UIColor, text : String, position : CGRect, alignment : NSTextAlignment, fontSize : CGFloat) -> UILabel{
        let labelCreate = UILabel.init()
        labelCreate.frame = position
        labelCreate.text = text
        labelCreate.textColor = color
        labelCreate.textAlignment = alignment
        labelCreate.isUserInteractionEnabled = true;
        
        // font
        labelCreate.font = labelCreate.font.withSize(fontSize)
        
        //        // underline
        //        if(isUnderline){
        //            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        //            let underlineAttributedString = NSAttributedString(string: text, attributes: underlineAttribute)
        //            labelCreate.attributedText = underlineAttributedString
        //        }
        
        return labelCreate
    }
    
    // Textfield
    func createTextFieldWithPlaceholder(placeHolder : String, position : CGRect) -> UITextField{
        let labelCreate = UITextField.init()
        labelCreate.frame = position
        labelCreate.placeholder = placeHolder
        labelCreate.layer.borderColor = UIColor.gray.cgColor
        labelCreate.layer.borderWidth = 0.5
        labelCreate.layer.cornerRadius = 2
        labelCreate.autocorrectionType = .no
        return labelCreate
    }
    
    func showErrorAlert(errorMsg : String, isAction : Bool){
        var msg = "Information"
        var ac = UIAlertController(title: msg, message: errorMsg, preferredStyle: .alert)
        if(isAction){
            ac = UIAlertController(title: msg, message: errorMsg, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                self.okToDelete()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
                self.noToDelete()
            }
            
            // Add the actions
            ac.addAction(okAction)
            ac.addAction(cancelAction)
        }else{
            ac.addAction(UIAlertAction(title: "OK", style: .default))
        }
        self.present(ac, animated:  true)
    }
    
    func okToDelete() {
        print("delete ok")
    }
    
    func noToDelete() {
        print("delete no")
    }
    
    func showUIBoxSave(){
        bgTransparant = UIView.init()
        bgTransparant.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight);
        bgTransparant.backgroundColor =  UIColor(white: 0, alpha: 0.5)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
        bgTransparant.addGestureRecognizer(tap)
        bgTransparant.isUserInteractionEnabled = true
        self.view.addSubview(bgTransparant)
        
        
        let boxAdd = UIView.init()
        boxAdd.frame = CGRect(x: 30, y: 100, width: screenWidth - 60, height: screenHeight - 420);
        boxAdd.backgroundColor =  UIColor.white
        boxAdd.layer.cornerRadius = 10
        bgTransparant.addSubview(boxAdd)
        
        // firstname
        boxAdd.addSubview(self.createLabelWithText(color: UIColor.black, text: "First Name", position: CGRect(x:20,y:20,width:100,height:20), alignment: NSTextAlignment.left, fontSize: 15))
        firstNameVal = self.createTextFieldWithPlaceholder(placeHolder: "", position: CGRect(x:130,y:15,width:170,height:30))
        boxAdd.addSubview(firstNameVal)
        
        // lastname
        boxAdd.addSubview(self.createLabelWithText(color: UIColor.black, text: "Last Name", position: CGRect(x:20,y:60,width:100,height:20), alignment: NSTextAlignment.left, fontSize: 15))
        lastNameVal = self.createTextFieldWithPlaceholder(placeHolder: "", position: CGRect(x:130,y:55,width:170,height:30))
        boxAdd.addSubview(lastNameVal)
        
        // age
        boxAdd.addSubview(self.createLabelWithText(color: UIColor.black, text: "Age", position: CGRect(x:20,y:100,width:100,height:20), alignment: NSTextAlignment.left, fontSize: 15))
        ageVal = self.createTextFieldWithPlaceholder(placeHolder: "", position: CGRect(x:130,y:95,width:170,height:30))
        boxAdd.addSubview(ageVal)
        
        // photo url
        boxAdd.addSubview(self.createLabelWithText(color: UIColor.black, text: "Photo URL", position: CGRect(x:20,y:140,width:100,height:20), alignment: NSTextAlignment.left, fontSize: 15))
        urlVal = self.createTextFieldWithPlaceholder(placeHolder: "", position: CGRect(x:130,y:135,width:170,height:30))
        boxAdd.addSubview(urlVal)
        
        // button save
        boxAdd.addSubview(self.createButtonWithText(buttonColor: UIColor.red, buttonText: "Save", position: CGRect(x:60,y:185,width:boxAdd.frame.size.width - 120,height:30), buttonTag: 2))
    }
    
    func touchHappen(_ sender: UITapGestureRecognizer) {
        bgTransparant.removeFromSuperview()
    }
}

extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
}

// =========================================== API SETTING ===========================================
// ===================================================================================================


let urlBase = "https://simple-contact-crud.herokuapp.com/"

func initAPIDataWithType(APIType : String, isGetById : Bool, completion: @escaping (Int) -> ()){
    let urlCombineType = urlBase + APIType
    let url = URL(string: urlCombineType)
    
    let session = URLSession.shared
    let task = session.dataTask(with: url!) { (data, _, _) -> Void in
        if let data = data {
            do{
                let string = String(data: data, encoding: String.Encoding.utf8)
                let modified = string?.replace(target: "\\\\", withString: "")
                dictAPI = convertToDictionary(text: modified!) as! NSDictionary
                let checkError = dictAPI["error"]
                if(checkError == nil){
                    DispatchQueue.main.async {
                        let dictApiTemp = dictAPI["data"]! as AnyObject
                        var arrayOfMutatingDictionaries = [[String : Any]]()
                        for _ in 1...dictApiTemp.count! {
                            let item: [String: Any] = [:]
                            
                            // get existing items, or create new array if doesn't exist
                            arrayOfMutatingDictionaries = dictAPI["data"] as? [[String: Any]] ?? [[String: Any]]()
                            
                            // append the item
                            arrayOfMutatingDictionaries.append(item)
                            
                            // replace back into `data`
                            //                        data["items"] = existingItems
                        }
                        errorMessage = dictAPI["message"] as! String
                        if(isGetById){
                            arrayGlobalCallbackByIdAPI = [[String: Any]()]
                            let dict = dictAPI["data"]! as Any
                            arrayGlobalCallbackByIdAPI.append(dict as! [String : Any])
                        }else{
                            arrayGlobalCallbackAPI = arrayOfMutatingDictionaries
                        }
                        completion(1)
                    }
                }else{
                    errorMessage = dictAPI["message"] as! String
                    errorCodeString = String(dictAPI["statusCode"] as! Int64)
                    completion(0) // or return an error code
                    return
                }
            }
        }else{
            errorMessage = "No Internet Connection"
            errorCodeString = "";
            completion(0) // or return an error code
            return
        }
    }
    task.resume()
}

func sendAPIData(dataDict : NSDictionary, urlType : String, serviceType : String, completion: @escaping (Int) -> ()){
    
    // post the data
    let urlCombineType = urlBase + urlType
    
    if let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: .prettyPrinted) {
        
        let url = NSURL(string: urlCombineType)!
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = serviceType
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            if error != nil{
//                print(error?.localizedDescription as Any)
                errorMessage = error?.localizedDescription as! String
                completion(0)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                
                if json != nil {
                    DispatchQueue.main.async {
                        errorMessage = json!["message"] as! String
                        completion(1)
                        return
                    }
                }
            } catch let error as NSError {
                print(error)
                errorMessage = error.localizedDescription
                completion(0)
                return
            }
        }
        task.resume()
    }

}


func convertToDictionary(text: String) -> Any? {
    
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? Any
        } catch {
            print(error.localizedDescription)
        }
    }
    
    return nil
    
}

func callbackAPI(dictionary : NSDictionary){
    print("callback not implemented")
}




