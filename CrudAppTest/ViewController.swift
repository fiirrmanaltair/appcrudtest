//
//  ViewController.swift
//  CrudAppTest
//
//  Created by Firman on 30/10/18.
//  Copyright © 2018 Firman App. All rights reserved.
//

import UIKit

var myArray: NSArray = ["No Data"]
var arrayOfMutatingDictionaries = [[String : Int]]()
var myTableView: UITableView!
var isSaveData: Bool = false
var indexIDToDelete : Int = 0
let refreshController:UIRefreshControl = UIRefreshControl()
var isUpdateData: Bool = false

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewDidLoad() {
        if(!isSaveData){
            super.viewDidLoad()
            self.view.backgroundColor = UIColor.white
            
            //        arrayGlobalCallbackAPI = myArray as! [[String : Any]]
            self.setupUI()
            initAPIDataWithType(APIType: "contact", isGetById: false){ code in
                if(code == 1){
                    myArray = arrayGlobalCallbackAPI as NSArray
                    myTableView.reloadData()
                }else{
                    self.showErrorAlert(errorMsg: errorMessage + " \(errorCodeString)",isAction: false)
                }
            }
        }
    }
    
    // =========================================== UI SETTING ===========================================
    
    func setupUI(){
        // create button add
        self.view.addSubview(createButtonWithText(buttonColor: UIColor.red, buttonText: "Add Hero", position: CGRect(x: 30, y: 50, width: 100, height: 50), buttonTag: 1))
        
        // create text
        self.view.addSubview(createLabelWithText(color: UIColor.black, text: "List Contact", position: CGRect(x: 30, y:120, width: screenWidth, height: 20), alignment: NSTextAlignment.left, fontSize: 20))
        
        // create text
        self.view.addSubview(createLabelWithText(color: UIColor.red, text: "Swipe for Delete", position: CGRect(x: screenWidth - 110, y:120, width: 80, height: 20), alignment: NSTextAlignment.left, fontSize: 10))
        
        createTableView()
    }
    
    func createTableView(){
        myTableView = UITableView(frame: CGRect(x: 30, y: 160, width: screenWidth - 60, height: screenHeight - 180))
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.layer.borderColor = UIColor.black.cgColor
        myTableView.layer.borderWidth = 2
        myTableView.layer.cornerRadius = 5
        self.view.addSubview(myTableView)
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        myTableView.addSubview(refreshController) // not required when using UITableViewController
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexIDToDelete = indexPath.row
        callDataByRow()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayGlobalCallbackAPI.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
//        if (!cell) {
////            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
//        }
        if arrayGlobalCallbackAPI[indexPath.row]["firstName"] != nil {
            let combineName = arrayGlobalCallbackAPI[indexPath.row]["firstName"] as! String
            let testCombine = combineName.appendingFormat(" \(arrayGlobalCallbackAPI[indexPath.row]["lastName"]!)")
            let completeCombine = testCombine.appendingFormat(" Age \(arrayGlobalCallbackAPI[indexPath.row]["age"]!)")
            cell.textLabel!.text = "\(completeCombine)"
            
            let catPictureURL = URL(string: arrayGlobalCallbackAPI[indexPath.row]["photo"] as! String)!
            let pictureData = NSData(contentsOf: catPictureURL as URL)
            if(pictureData != nil){
                let catPicture = UIImage(data: pictureData! as Data)
                cell.imageView?.image = catPicture
            }else{
                cell.imageView?.image = UIImage(named: "noImage")
            }
        } else {
            print("Doesn’t contain a value.")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            indexIDToDelete = indexPath.row
            showErrorAlert(errorMsg: "Anda yakin untuk delete data ini?", isAction: true)
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        initAPIDataWithType(APIType: "contact", isGetById: false){ code in
            if(code == 1){
                myArray = arrayGlobalCallbackAPI as NSArray
                myTableView.reloadData()
                refreshController.endRefreshing()
                self.showErrorAlert(errorMsg: "Data Refreshed",isAction: false)
            }else{
                self.showErrorAlert(errorMsg: errorMessage + " \(errorCodeString)",isAction: false)
            }
        }
    }
    
    // =========================================== API SETTING ===========================================
    
    func callDataByRow(){
        isUpdateData = true;
        let combineName = arrayGlobalCallbackAPI[indexIDToDelete]["id"] as! String
        let urlCall = "contact/" + combineName
        initAPIDataWithType(APIType: urlCall, isGetById: true){ code in
            if(code == 1){
//                print(arrayGlobalCallbackByIdAPI)
                self.showUIBoxSave()
                print(arrayGlobalCallbackByIdAPI[1]["firstName"]!)
                firstNameVal.text = arrayGlobalCallbackByIdAPI[1]["firstName"] as! NSString as String
                lastNameVal.text = arrayGlobalCallbackByIdAPI[1]["lastName"] as! NSString as String
                ageVal.text = String(arrayGlobalCallbackByIdAPI[1]["age"] as! Int64)
                urlVal.text = arrayGlobalCallbackByIdAPI[1]["photo"] as! NSString as String
            }else{
                self.showErrorAlert(errorMsg: errorMessage, isAction: false)
            }
        }
    }
    
    // ============================================= BUTTON CALLBACK ==========================================
    override func okToDelete() {
        let combineName = arrayGlobalCallbackAPI[indexIDToDelete]["id"] as! String
        let urlCall = "contact/" + combineName
        let dict = ["id" : combineName] as NSDictionary
        sendAPIData(dataDict: dict, urlType: urlCall, serviceType: "DELETE"){ code in
            if(code == 1){
                self.showErrorAlert(errorMsg: errorMessage,isAction: false)
                initAPIDataWithType(APIType: "contact", isGetById: false){ code in
                    if(code == 1){
                        myArray = arrayGlobalCallbackAPI as NSArray
                        myTableView.reloadData()
                    }else{
                        self.showErrorAlert(errorMsg: errorMessage, isAction: false)
                    }
                }
            }else{
                self.showErrorAlert(errorMsg: errorMessage, isAction: false)
            }
        }
    }
    
    override func buttonAction(sender:UIButton!) {
        let btnsendtag : UIButton = sender
        if(btnsendtag.tag == 1) {
            isUpdateData = false;
            showUIBoxSave()
        }else if(btnsendtag.tag == 2) {
            if(firstNameVal.text == "" || lastNameVal.text == "" || ageVal.text == "" || urlVal.text == ""){
                self.showErrorAlert(errorMsg: "lengkapi data anda",isAction: false)
            }else{
                var stringTypeService = "POST"
                var combineLink = "contact"
                let dict = ["firstName": firstNameVal.text as Any, "lastName":lastNameVal.text as Any, "age":ageVal.text as Any, "photo":urlVal.text as Any] as NSDictionary
                if(isUpdateData){
                    stringTypeService = "PUT"
                    combineLink = "contact/\(arrayGlobalCallbackByIdAPI[1]["id"]!)"
                }
                
                sendAPIData(dataDict: dict, urlType: combineLink, serviceType: stringTypeService){ code in
                    if(code == 1){
                        DispatchQueue.main.async {
                            self.showErrorAlert(errorMsg: errorMessage, isAction: false)
                            bgTransparant.removeFromSuperview()
                            initAPIDataWithType(APIType: "contact", isGetById: false){ code in
                                if(code == 1){
                                    myArray = arrayGlobalCallbackAPI as NSArray
                                    myTableView.reloadData()
                                }else{
                                    self.showErrorAlert(errorMsg: errorMessage, isAction: false)
                                }
                            }
                        }
                    }else{
                        self.showErrorAlert(errorMsg: errorMessage, isAction: false)
                    }
                }
            }
        }else{
            print("Button action not implemented")
        }
    }
    
    // =========================================== CALLBACK SYSTEM =======================================
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

